# -*- encoding: utf-8 -*-
# stub: refinerycms-settings 3.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "refinerycms-settings"
  s.version = "3.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Philip Arndt", "U\u{123}is Ozols"]
  s.date = "2014-10-02"
  s.description = "Adds programmer creatable, user editable settings."
  s.email = "info@refinerycms.com"
  s.files = [".gitignore", ".rspec", ".travis.yml", "Gemfile", "Rakefile", "app/controllers/refinery/admin/settings_controller.rb", "app/helpers/refinery/admin/settings_helper.rb", "app/models/refinery/setting.rb", "app/views/refinery/admin/settings/_actions.html.erb", "app/views/refinery/admin/settings/_form.html.erb", "app/views/refinery/admin/settings/_records.html.erb", "app/views/refinery/admin/settings/_setting.html.erb", "app/views/refinery/admin/settings/_settings.html.erb", "app/views/refinery/admin/settings/edit.html.erb", "app/views/refinery/admin/settings/index.html.erb", "app/views/refinery/admin/settings/new.html.erb", "config/locales/bg.yml", "config/locales/cs.yml", "config/locales/da.yml", "config/locales/de.yml", "config/locales/el.yml", "config/locales/en.yml", "config/locales/es.yml", "config/locales/fi.yml", "config/locales/fr.yml", "config/locales/it.yml", "config/locales/ja.yml", "config/locales/ko.yml", "config/locales/lolcat.yml", "config/locales/lt.yml", "config/locales/lv.yml", "config/locales/nb.yml", "config/locales/nl.yml", "config/locales/pl.yml", "config/locales/pt-BR.yml", "config/locales/rs.yml", "config/locales/ru.yml", "config/locales/sk.yml", "config/locales/sl.yml", "config/locales/sv.yml", "config/locales/vi.yml", "config/locales/zh-CN.yml", "config/locales/zh-TW.yml", "config/routes.rb", "db/migrate/20100913234710_create_refinerycms_settings_schema.rb", "db/migrate/20130414130143_add_slug_to_refinery_settings.rb", "db/migrate/20130422105953_add_title_to_refinery_settings.rb", "lib/generators/refinery/settings_generator.rb", "lib/generators/refinery/templates/config/initializers/refinery/settings.rb.erb", "lib/refinery/settings.rb", "lib/refinery/settings/configuration.rb", "lib/refinery/settings/engine.rb", "lib/refinerycms-settings.rb", "license.md", "readme.md", "refinerycms-settings.gemspec", "script/rails", "spec/factories/settings.rb", "spec/features/refinery/admin/settings_spec.rb", "spec/models/refinery/setting_spec.rb", "spec/routing/settings_routing_spec.rb", "spec/spec_helper.rb", "tasks/rspec.rake"]
  s.homepage = "http://refinerycms.com"
  s.licenses = ["MIT"]
  s.rubyforge_project = "refinerycms"
  s.rubygems_version = "2.2.2"
  s.summary = "Settings engine for Refinery CMS"
  s.test_files = ["spec/factories/settings.rb", "spec/features/refinery/admin/settings_spec.rb", "spec/models/refinery/setting_spec.rb", "spec/routing/settings_routing_spec.rb", "spec/spec_helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<refinerycms-core>, ["~> 3.0.0"])
      s.add_runtime_dependency(%q<friendly_id>, ["~> 5.0.1"])
    else
      s.add_dependency(%q<refinerycms-core>, ["~> 3.0.0"])
      s.add_dependency(%q<friendly_id>, ["~> 5.0.1"])
    end
  else
    s.add_dependency(%q<refinerycms-core>, ["~> 3.0.0"])
    s.add_dependency(%q<friendly_id>, ["~> 5.0.1"])
  end
end
